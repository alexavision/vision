FROM microsoft/dotnet:latest

# Set environment variables
ENV ASPNETCORE_ENVIRONMENT="Development"
ENV KESTREL_UseUrls="true"
ENV KESTREL_ListenUrls="http://*:6000/"
ENV IDENTITY_SigningCn="CN=SAPPHIRE-VisionHost"

# Copy files to app directory
COPY /src/FluiTec/FluiTec.Vision.Server.Host.ConsoleHost /app/vision

# Set working directory
WORKDIR /app/vision

# Restore NuGet packages
#RUN ["dotnet", "restore"]

# Build the app
#RUN ["dotnet", "build"]

# Open port
#EXPOSE 6000/tcp

# Run the app
#ENTRYPOINT ["dotnet", "run"]