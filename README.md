# README #

Vision

### What is this repository for? ###

* Remote Controller for a Computer and a SmartHome featuring Alexa-Integration
* Early project

### How do I get set up? ###

* Install IIS
* Install IIS-URL-Rewrite
* Install the VisionHost and configure some ListeningPoints
* Configure IIS-URL-Rewrite to forward calls to VisionHost
* Configure Alexa-Skills (Jarvis+Friday)
* Install JarvisHost and/or FridayHost


### Help? ###
* Feel free to contact me. I'd be happy about some support and instructional help.