﻿using FluiTec.AppFx.Logging.Settings;
using FluiTec.AppFx.Service;

namespace FluiTec.AppFx.Logging.Services
{
	/// <summary>	Interface for logger settings service. </summary>
	public interface ILoggerSettingsService : ISettingsService<ILoggerSettings>
	{
	}
}