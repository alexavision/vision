﻿using FluiTec.AppFx.Service;
using FluiTec.Vision.Nancy.Authentication.Settings;

namespace FluiTec.Vision.Nancy.Authentication.Services
{
	/// <summary>	Interface for authentication settings service. </summary>
	public interface IAuthenticationSettingsService : ISettingsService<IAuthenticationSettings>
	{
	}
}