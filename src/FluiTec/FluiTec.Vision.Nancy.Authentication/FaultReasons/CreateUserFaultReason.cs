﻿namespace FluiTec.Vision.Nancy.Authentication.FaultReasons
{
	/// <summary>	Values that represent create user fault reasons. </summary>
	public enum CreateUserFaultReason
	{
		/// <summary>	An enum constant representing the existing user option. </summary>
		ExistingUser,

		/// <summary>	An enum constant representing the invalid data option. </summary>
		InvalidData,

		/// <summary>	An enum constant representing the unknown option. </summary>
		Unknown
	}
}