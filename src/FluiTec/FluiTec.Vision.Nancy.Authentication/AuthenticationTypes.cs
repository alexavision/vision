﻿namespace FluiTec.Vision.Nancy.Authentication
{
	/// <summary>	An authentication types. </summary>
	public static class AuthenticationTypes
	{
		/// <summary>	The form credentials. </summary>
		public static string FormCredentials => "FormCredentials";

		/// <summary>	The identifier cookie. </summary>
		public static string IdentifierCookie => "IdentifierCookie";

		/// <summary>	The owin cookie. </summary>
		public static string OwinCookie => "OwinCookie";
	}
}