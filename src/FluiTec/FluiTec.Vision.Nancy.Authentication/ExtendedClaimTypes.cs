﻿namespace FluiTec.Vision.Nancy.Authentication
{
	/// <summary>	An extended claim name. </summary>
	public static class ExtendedClaimTypes
	{
		/// <summary>	Unique identifier. </summary>
		public static string UniqueId => "uid";
	}
}