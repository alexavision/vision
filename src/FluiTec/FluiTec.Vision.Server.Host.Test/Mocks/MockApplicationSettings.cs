﻿using FluiTec.Vision.Server.Host.Settings;
using Microsoft.Extensions.Logging;

namespace FluiTec.Vision.Server.Host.Test.Mocks
{
    public class MockApplicationSettings : IApplicationSettings
    {
	    public string EncryptionKey { get; set; }
	    public string HmacKey { get; set; }
	    public ILoggerFactory LoggerFactory { get; }

	    public string DefaultConnectionString { get; }

	    public MockApplicationSettings()
	    {
		    LoggerFactory = new MockLoggerFactory();
		    DefaultConnectionString = "";
	    }
    }
}
