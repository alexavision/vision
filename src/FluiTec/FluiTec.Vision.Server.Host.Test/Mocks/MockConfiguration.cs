﻿using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;

namespace FluiTec.Vision.Server.Host.Test.Mocks
{
	public class MockConfiguration : IConfiguration
	{
		public IConfigurationSection GetSection(string key)
		{
			return null;
		}

		public IEnumerable<IConfigurationSection> GetChildren()
		{
			return null;
		}

		public IChangeToken GetReloadToken()
		{
			return null;
		}

		public string this[string key]
		{
			get { return null; }
			set {  }
		}
	}
}