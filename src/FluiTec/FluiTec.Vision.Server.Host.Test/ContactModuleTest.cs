﻿using FluiTec.Vision.Server.Host.Test.Mocks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nancy;
using Nancy.Testing;

namespace FluiTec.Vision.Server.Host.Test
{
	/// <summary>	(Unit Test Class) a contact module test. </summary>
	[TestClass]
	public class ContactModuleTest
    {
	    /// <summary>	The bootstrapper. </summary>
	    private readonly DefaultNancyBootstrapper _bootstrapper;

	    /// <summary>	Default constructor. </summary>
	    public ContactModuleTest()
	    {
			IServiceCollection collection = new ServiceCollection();
		    _bootstrapper = new VisionNancyBootstrapper(new DefaultServiceProviderFactory().CreateServiceProvider(collection), new MockLoggerFactory());
		}

	    /// <summary>	(Unit Test Method) provides imprint site. </summary>
	    [TestMethod]
	    public void ProvidesImprintSite()
	    {
		    var browser = new Browser(_bootstrapper);

		    var result = browser.Get("/Contact/Imprint", with => { with.HttpRequest(); }).Result;

		    Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
	    }

	    /// <summary>	(Unit Test Method) provides terms of use site. </summary>
	    [TestMethod]
	    public void ProvidesTermsOfUseSite()
	    {
		    var browser = new Browser(_bootstrapper);

		    var result = browser.Get("/Contact/TermsOfUse", with => { with.HttpRequest(); }).Result;

		    Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
	    }

	    /// <summary>	(Unit Test Method) provides privacy site. </summary>
	    [TestMethod]
	    public void ProvidesPrivacySite()
	    {
		    var browser = new Browser(_bootstrapper);

		    var result = browser.Get("/Contact/Privacy", with => { with.HttpRequest(); }).Result;

		    Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
	    }
	}
}
