﻿using FluiTec.Vision.Server.Host.Test.Mocks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nancy;
using Nancy.Testing;

namespace FluiTec.Vision.Server.Host.Test
{
	[TestClass]
	public class AccountModuleTest
	{
		/// <summary>	The bootstrapper. </summary>
		private readonly DefaultNancyBootstrapper _bootstrapper;

		/// <summary>	Default constructor. </summary>
		public AccountModuleTest()
		{
			IServiceCollection collection = new ServiceCollection();
			_bootstrapper = new VisionNancyBootstrapper(new DefaultServiceProviderFactory().CreateServiceProvider(collection), new MockLoggerFactory());
		}

		/// <summary>	(Unit Test Method) provides imprint site. </summary>
		[TestMethod]
		public void ProvidesLoginSite()
		{
			var browser = new Browser(_bootstrapper);

			var result = browser.Get("/Account/Login", with => { with.HttpRequest(); }).Result;

			Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
		}

		/// <summary>	(Unit Test Method) provides register site. </summary>
		[TestMethod]
		public void ProvidesRegisterSite()
		{
			var browser = new Browser(_bootstrapper);

			var result = browser.Get("/Account/Register", with => { with.HttpRequest(); }).Result;

			Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
		}

		/// <summary>	(Unit Test Method) provides register site. </summary>
		[TestMethod]
		public void DeniesAccessToManageSite()
		{
			var browser = new Browser(_bootstrapper);

			var result = browser.Get("/Account/Manage", with => { with.HttpRequest(); }).Result;

			Assert.AreEqual(HttpStatusCode.TemporaryRedirect, result.StatusCode);
		}
	}
}