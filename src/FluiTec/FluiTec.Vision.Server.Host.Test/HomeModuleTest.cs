﻿using FluiTec.Vision.Server.Host.Test.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nancy;
using Nancy.Testing;

namespace FluiTec.Vision.Server.Host.Test
{
	/// <summary>	(Unit Test Class) a home module test. </summary>
	[TestClass]
	public class HomeModuleTest
	{
		private readonly DefaultNancyBootstrapper _bootstrapper;

		/// <summary>	Default constructor. </summary>
		public HomeModuleTest()
		{
			//_bootstrapper = new VisionNancyBootstrapper(new MockApplicationSettings(), new MockConfiguration());
		}

		/// <summary>	(Unit Test Method) provides root site. </summary>
		[TestMethod]
		public void ProvidesIndexSite()
		{
			var browser = new Browser(_bootstrapper);

			var result = browser.Get("/", with => { with.HttpRequest(); }).Result;

			Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
		}

		/// <summary>	(Unit Test Method) redirects on home directory. </summary>
		[TestMethod]
		public void RedirectsOnHomeDirectory()
		{
			var browser = new Browser(_bootstrapper);

			var result = browser.Get("/Home", with => { with.HttpRequest(); }).Result;

			Assert.AreEqual(HttpStatusCode.MovedPermanently, result.StatusCode);
		}

		/// <summary>	(Unit Test Method) redirects on home index file. </summary>
		[TestMethod]
		public void RedirectsOnHomeIndexFile()
		{
			var browser = new Browser(_bootstrapper);

			var result = browser.Get("/Home/Index", with => { with.HttpRequest(); }).Result;

			Assert.AreEqual(HttpStatusCode.MovedPermanently, result.StatusCode);
		}

		/// <summary>	(Unit Test Method) provides about site. </summary>
		[TestMethod]
		public void ProvidesAboutSite()
		{
			var browser = new Browser(_bootstrapper);

			var result = browser.Get("/Home/About", with => { with.HttpRequest(); }).Result;

			Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
		}
	}
}