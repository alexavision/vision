﻿using FluiTec.AppFx.Service;
using FluiTec.Vision.Server.Host.Settings;

namespace FluiTec.Vision.Server.Host.Services
{
	/// <summary>	Interface for application settings service. </summary>
	public interface IApplicationSettingsService : ISettingsService<IApplicationSettings>
	{
	}
}