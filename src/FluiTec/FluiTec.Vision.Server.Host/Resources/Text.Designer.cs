﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.42000
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FluiTec.Vision.Server.Host.Resources {
    using System;
    using System.Reflection;
    
    
    /// <summary>
    ///   Eine stark typisierte Ressourcenklasse zum Suchen von lokalisierten Zeichenfolgen usw.
    /// </summary>
    // Diese Klasse wurde von der StronglyTypedResourceBuilder automatisch generiert
    // -Klasse über ein Tool wie ResGen oder Visual Studio automatisch generiert.
    // Um einen Member hinzuzufügen oder zu entfernen, bearbeiten Sie die .ResX-Datei und führen dann ResGen
    // mit der /str-Option erneut aus, oder Sie erstellen Ihr VS-Projekt neu.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Text {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Text() {
        }
        
        /// <summary>
        ///   Gibt die zwischengespeicherte ResourceManager-Instanz zurück, die von dieser Klasse verwendet wird.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("FluiTec.Vision.Server.Host.Resources.Text", typeof(Text).GetTypeInfo().Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Überschreibt die CurrentUICulture-Eigenschaft des aktuellen Threads für alle
        ///   Ressourcenzuordnungen, die diese stark typisierte Ressourcenklasse verwenden.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die About ähnelt.
        /// </summary>
        internal static string About {
            get {
                return ResourceManager.GetString("About", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Alexa ähnelt.
        /// </summary>
        internal static string AlexaFunction {
            get {
                return ResourceManager.GetString("AlexaFunction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Vision was built with for speech-control like alexa. ähnelt.
        /// </summary>
        internal static string AlexaFunctionDescription {
            get {
                return ResourceManager.GetString("AlexaFunctionDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die FluiTech:Vision ähnelt.
        /// </summary>
        internal static string ApplicationName {
            get {
                return ResourceManager.GetString("ApplicationName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Computer control ähnelt.
        /// </summary>
        internal static string ComputerControlFunction {
            get {
                return ResourceManager.GetString("ComputerControlFunction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Control your computer, far  away from mouse and keyboard. ähnelt.
        /// </summary>
        internal static string ComputerControlFunctionDescription {
            get {
                return ResourceManager.GetString("ComputerControlFunctionDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Copyright by Achim Schnell. All rights reserverd. ähnelt.
        /// </summary>
        internal static string Copyright {
            get {
                return ResourceManager.GetString("Copyright", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Design by ähnelt.
        /// </summary>
        internal static string DesignBy {
            get {
                return ResourceManager.GetString("DesignBy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Soon to come ähnelt.
        /// </summary>
        internal static string FutureFunction {
            get {
                return ResourceManager.GetString("FutureFunction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die All functions will soon be available using google assistant. ähnelt.
        /// </summary>
        internal static string FutureFunctionDescription {
            get {
                return ResourceManager.GetString("FutureFunctionDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Home ähnelt.
        /// </summary>
        internal static string Home {
            get {
                return ResourceManager.GetString("Home", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Imprint ähnelt.
        /// </summary>
        internal static string Imprint {
            get {
                return ResourceManager.GetString("Imprint", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die The voice control for computer and smart home ähnelt.
        /// </summary>
        internal static string LandingHeader {
            get {
                return ResourceManager.GetString("LandingHeader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Learn more ähnelt.
        /// </summary>
        internal static string LearnMore {
            get {
                return ResourceManager.GetString("LearnMore", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Login ähnelt.
        /// </summary>
        internal static string Login {
            get {
                return ResourceManager.GetString("Login", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Logout ähnelt.
        /// </summary>
        internal static string Logout {
            get {
                return ResourceManager.GetString("Logout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Privacy ähnelt.
        /// </summary>
        internal static string Privacy {
            get {
                return ResourceManager.GetString("Privacy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Register ähnelt.
        /// </summary>
        internal static string Register {
            get {
                return ResourceManager.GetString("Register", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die SmartHome ähnelt.
        /// </summary>
        internal static string SmartHomeFunction {
            get {
                return ResourceManager.GetString("SmartHomeFunction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Control your smart home - with custom routines. ähnelt.
        /// </summary>
        internal static string SmartHomeFunctionDescription {
            get {
                return ResourceManager.GetString("SmartHomeFunctionDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Steam ähnelt.
        /// </summary>
        internal static string SteamFunction {
            get {
                return ResourceManager.GetString("SteamFunction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die You can launch/close  your games with your voice. ähnelt.
        /// </summary>
        internal static string SteamFunctionDescription {
            get {
                return ResourceManager.GetString("SteamFunctionDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Terms of use ähnelt.
        /// </summary>
        internal static string TermsOfUse {
            get {
                return ResourceManager.GetString("TermsOfUse", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die VLC Player ähnelt.
        /// </summary>
        internal static string VlcFunction {
            get {
                return ResourceManager.GetString("VlcFunction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Vision allows you to control your vlc player with your voice. ähnelt.
        /// </summary>
        internal static string VlcFunctionDescription {
            get {
                return ResourceManager.GetString("VlcFunctionDescription", resourceCulture);
            }
        }
    }
}
