﻿using System;
using System.Globalization;
using FluentValidation;
using FluiTec.AppFx.Authentication.Data;
using FluiTec.AppFx.Globalization.Services;
using FluiTec.Vision.Nancy;
using FluiTec.Vision.Nancy.Authentication.Forms;
using FluiTec.Vision.Nancy.Authentication.Forms.Services;
using FluiTec.Vision.Nancy.Authentication.Forms.Settings;
using FluiTec.Vision.Nancy.Authentication.Owin;
using FluiTec.Vision.Nancy.Authentication.Services;
using FluiTec.Vision.Nancy.Authentication.Settings;
using FluiTec.Vision.Server.Data;
using FluiTec.Vision.Server.Data.Mssql;
using FluiTec.Vision.Server.Host.Localization;
using FluiTec.Vision.Server.Host.Services;
using FluiTec.Vision.Server.Host.Settings;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Configuration;
using Nancy.Conventions;
using Nancy.Cryptography;
using Nancy.Culture;
using Nancy.TinyIoc;

namespace FluiTec.Vision.Server.Host
{
	/// <summary>	A vision bootstrapper. </summary>
	public class VisionNancyBootstrapper : CustomNancyBootstrapper
	{
		#region Constructors

		/// <summary>	Constructor. </summary>
		/// <exception cref="ArgumentNullException">
		///     Thrown when one or more required arguments are
		///     null.
		/// </exception>
		/// <param name="serviceProvider">	The application. </param>
		/// <param name="loggerFactory">  	The logget factory. </param>
		public VisionNancyBootstrapper(IServiceProvider serviceProvider, ILoggerFactory loggerFactory)
		{
			_serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
			_loggerFactory = loggerFactory ?? throw new ArgumentNullException(nameof(loggerFactory));
			_logger = _loggerFactory.CreateLogger(typeof(VisionNancyBootstrapper));

			_applicationSettings = serviceProvider.GetRequiredService<IApplicationSettingsService>().Get();
		}

		#endregion

		#region Nancy

		/// <summary>	Gets the internal configuration. </summary>
		/// <value>	The internal configuration. </value>
		protected override Func<ITypeCatalog, NancyInternalConfiguration> InternalConfiguration
		{
			get
			{
				_logger.LogInformation($"Configuring nancy to use '{nameof(CustomResourceAssemblyProvider)}' as ResourceAssemblyProvider...");
				return NancyInternalConfiguration.WithOverrides(x => x.ResourceAssemblyProvider =
					typeof(CustomResourceAssemblyProvider));
			}
		}

		/// <summary>	Configures the given environment. </summary>
		/// <param name="environment">	The environment. </param>
		public override void Configure(INancyEnvironment environment)
		{
			_logger.LogInformation("Configuring nancy for globalization...");

			var settings = _serviceProvider.GetRequiredService<IGlobalizationSettingsService>().Get();
			_logger.LogInformation("GlobalizationSettings loaded: {0}.", settings);

			environment.Globalization(settings.SupportedCultures, settings.DefaultCulture);

			ValidatorOptions.ResourceProviderType = typeof(ValidationResources);

			base.Configure(environment);
		}

		/// <summary>	Application startup. </summary>
		/// <param name="container">	The container. </param>
		/// <param name="pipelines">	The pipelines. </param>
		protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
		{
			// register the logger
			container.Register(_loggerFactory);

			// let the base class do it's job
			base.ApplicationStartup(container, pipelines);

			ConfigureDataService(container);
			ConfigureUserService(container);
			ConfigureEncryption(container);
			ConfigureOwinAuthentication(container);
			ConfigureFormsAuthentication(container, pipelines);
		}

		/// <summary>	Request startup. </summary>
		/// <param name="container">	The container. </param>
		/// <param name="pipelines">	The pipelines. </param>
		/// <param name="context">  	The context. </param>
		protected override void RequestStartup(TinyIoCContainer container, IPipelines pipelines, NancyContext context)
		{
			context.Trace.Items.Add(RequestExtensions.RequestIdKey, Guid.NewGuid());
			base.RequestStartup(container, pipelines, context);

			_logger.LogDebug("Request[{0}]: Configuring RequestCulture...", context.RequestId());

			var cultureService = container.Resolve<ICultureService>();
			var culture = cultureService.DetermineCurrentCulture(context);
			_logger.LogDebug("Request[{0}]: RequestCulture is {1}.", context.RequestId(), culture);

			CultureInfo.CurrentCulture = culture;
			CultureInfo.CurrentUICulture = culture;
		}

		/// <summary>	Configure conventions. </summary>
		/// <param name="nancyConventions">	The nancy conventions. </param>
		/// <remarks>
		///     Basically maps incoming routes to content-paths
		/// </remarks>
		protected override void ConfigureConventions(NancyConventions nancyConventions)
		{
			base.ConfigureConventions(nancyConventions);

			_logger.LogInformation("Configuring FileConventions...");
			nancyConventions.StaticContentsConventions.AddDirectory("js", "/Content/js", "js");
			nancyConventions.StaticContentsConventions.AddDirectory("images", "/Content/images", "jpg", "png");
			nancyConventions.StaticContentsConventions.AddDirectory("css", "/Content/css", "css", "htc", "js", "png");
			nancyConventions.StaticContentsConventions.AddDirectory("fonts", "/Content/fonts", "eot", "svg", "ttf", "woff",
				"otf");
		}

		#endregion

		#region Configuration

		/// <summary>	Configure owin authentication. </summary>
		/// <param name="container">	The container. </param>
		private void ConfigureOwinAuthentication(TinyIoCContainer container)
		{
			_logger.LogInformation("Configuring OwinAuthentication...");
			container.Register(_serviceProvider.GetRequiredService<IAuthenticationSettingsService>().Get());
			container.Register<IAuthenticationService, OwinAuthenticationService>();
		}

		/// <summary>	Configure data service. </summary>
		/// <param name="container">	The container. </param>
		private void ConfigureDataService(TinyIoCContainer container)
		{
			_logger.LogInformation("Configuring DataService...");
			container.Register<IVisionDataService>(
				(s, p) => new VisionDataService(_loggerFactory, _applicationSettings.DefaultConnectionString));
			container.Register<IAuthenticatingDataService>(
				(s, p) => new VisionDataService(_loggerFactory, _applicationSettings.DefaultConnectionString));
		}

		/// <summary>	Configure user service. </summary>
		/// <param name="container">	The container. </param>
		private void ConfigureUserService(TinyIoCContainer container)
		{
			_logger.LogInformation("Configuring UserService...");
			container.Register(_serviceProvider.GetRequiredService<IAuthenticationSettingsService>());
			container.Register<IUserService, UserService>();
		}

		/// <summary>	Configure encryption. </summary>
		/// <param name="container">	The container. </param>
		private void ConfigureEncryption(TinyIoCContainer container)
		{
			_logger.LogInformation("Configuring Encryption...");

			var config = new CryptographyConfiguration(
				new AesEncryptionProvider(
					new PassphraseKeyGenerator(
						_applicationSettings.EncryptionKey,
						new byte[] {5, 2, 7, 5, 9, 1, 2, 1})),
				new DefaultHmacProvider(
					new PassphraseKeyGenerator(
						_applicationSettings.HmacKey,
						new byte[] {2, 7, 3, 8, 1, 1, 8, 1})));

			container.Register(config);
		}

		/// <summary>	Configure forms authentication. </summary>
		/// <param name="container">	The container. </param>
		/// <param name="pipelines">	The pipelines. </param>
		private void ConfigureFormsAuthentication(TinyIoCContainer container, IPipelines pipelines)
		{
			_logger.LogInformation("Configuring FormsAuthentication...");
			container.Register(_serviceProvider.GetRequiredService<IFormsAuthenticationSettingsService>().Get());

			// enable forms-authentication
			pipelines.EnableFormsAuthentication(container, 
				container.Resolve<IFormsAuthenticationSettings>(),
				container.Resolve<IAuthenticationSettings>());
		}

		#endregion

		#region Fields

		/// <summary>	The service provider. </summary>
		private readonly IServiceProvider _serviceProvider;

		/// <summary>	The logger factory. </summary>
		private readonly ILoggerFactory _loggerFactory;

		/// <summary>	The application settings. </summary>
		private readonly IApplicationSettings _applicationSettings;

		/// <summary>	The logger. </summary>
		private readonly ILogger _logger;

		#endregion
	}
}