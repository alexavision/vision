﻿using Microsoft.Extensions.Logging;

namespace FluiTec.Vision.Server.Host.Settings
{
	/// <summary>	Interface for application settings. </summary>
	public interface IApplicationSettings
	{
		/// <summary>	Gets or sets the encryption key. </summary>
		/// <value>	The encryption key. </value>
		string EncryptionKey { get; set; }

		/// <summary>	Gets or sets the hmac key. </summary>
		/// <value>	The hmac key. </value>
		string HmacKey { get; set; }

		/// <summary>	Gets the default connection string. </summary>
		/// <value>	The default connection string. </value>
		string DefaultConnectionString { get; }
	}
}