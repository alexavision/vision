﻿using Microsoft.Extensions.Logging;

namespace FluiTec.Vision.Server.Host.Settings
{
	/// <summary>	An application settings. </summary>
	public class ApplicationSettings : IApplicationSettings
	{
		/// <summary>	Gets or sets the encryption key. </summary>
		/// <value>	The encryption key. </value>
		public string EncryptionKey { get; set; }

		/// <summary>	Gets or sets the hmac key. </summary>
		/// <value>	The hmac key. </value>
		public string HmacKey { get; set; }

		/// <summary>	Gets the default connection string. </summary>
		/// <value>	The default connection string. </value>
		public string DefaultConnectionString { get; set; }
	}
}