﻿using FluiTec.Vision.Server.Host.Resources;

namespace FluiTec.Vision.Server.Host.Localization
{
    /// <summary>	A validation resources. </summary>
    public class ValidationResources
    {
	    // ReSharper disable InconsistentNaming
	    /// <summary>	The email error. </summary>
	    public static string email_error => ErrorMessages.email_error;

	    /// <summary>	The equal error. </summary>
	    public static string equal_error => ErrorMessages.equal_error;

	    /// <summary>	The exact length error. </summary>
	    public static string exact_length_error => ErrorMessages.exact_length_error;

	    /// <summary>	The exclusivebetween error. </summary>
	    public static string exclusivebetween_error => ErrorMessages.exclusivebetween_error;

	    /// <summary>	The greaterthan error. </summary>
	    public static string greaterthan_error => ErrorMessages.greaterthan_error;

	    /// <summary>	The greaterthanorequal error. </summary>
	    public static string greaterthanorequal_error => ErrorMessages.greaterthanorequal_error;

	    /// <summary>	The inclusivebetween error. </summary>
	    public static string inclusivebetween_error => ErrorMessages.inclusivebetween_error;

	    /// <summary>	The length error. </summary>
	    public static string length_error => ErrorMessages.length_error;

	    /// <summary>	The lessthan error. </summary>
	    public static string lessthan_error => ErrorMessages.lessthan_error;

	    /// <summary>	The lessthanorequal error. </summary>
	    public static string lessthanorequal_error => ErrorMessages.lessthanorequal_error;

		/// <summary>	The notempty error. </summary>
		public static string notempty_error => ErrorMessages.notempty_error;

	    /// <summary>	The notequal error. </summary>
	    public static string notequal_error => ErrorMessages.notequal_error;

	    /// <summary>	The notnull error. </summary>
	    public static string notnull_error => ErrorMessages.notnull_error;

	    /// <summary>	The predicate error. </summary>
	    public static string predicate_error => ErrorMessages.predicate_error;

	    /// <summary>	The RegEx error. </summary>
	    public static string regex_error => ErrorMessages.regex_error;

	    // ReSharper restore InconsistentNaming
    }
}
