﻿using FluiTec.AppFx.Service;
using FluiTec.Vision.Nancy.Authentication.Forms.Settings;

namespace FluiTec.Vision.Nancy.Authentication.Forms.Services
{
	/// <summary>	Interface for forms authentication settings service. </summary>
	public interface IFormsAuthenticationSettingsService : ISettingsService<IFormsAuthenticationSettings>
	{
	}
}