﻿using FluiTec.Vision.Nancy.Authentication;
using Microsoft.AspNetCore.Builder;

namespace FluiTec.Vision.Server.Host.ConsoleHost.Extensions
{
	/// <summary>	A cookie extension. </summary>
	public static class CookieExtension
	{
		/// <summary>
		///     An IApplicationBuilder extension method that configure cookie authentication.
		/// </summary>
		/// <param name="application">	The application to act on. </param>
		/// <returns>	An IApplicationBuilder. </returns>
		public static IApplicationBuilder ConfigureCookieAuthentication(this IApplicationBuilder application)
		{
			application.UseCookieAuthentication(new CookieAuthenticationOptions
			{
				AuthenticationScheme = AuthenticationTypes.OwinCookie,
				AutomaticAuthenticate = true,
				AutomaticChallenge = false
			});

			return application;
		}
	}
}