﻿using System;
using System.Linq;
using FluiTec.AppFx.Globalization.Services;
using FluiTec.AppFx.Logging.Services;
using FluiTec.AppFx.Proxy.Services;
using FluiTec.AppFx.Signing.Services;
using FluiTec.Vision.Nancy.Authentication.Forms.Services;
using FluiTec.Vision.Nancy.Authentication.Owin.Settings;
using FluiTec.Vision.Nancy.Authentication.Services;
using FluiTec.Vision.Server.Host.ConsoleHost.Extensions;
using FluiTec.Vision.Server.Host.ConsoleHost.Services;
using FluiTec.Vision.Server.Host.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace FluiTec.Vision.Server.Host.ConsoleHost
{
	/// <summary>	A startup-class used to initialize hosting. </summary>
	public class Startup
	{
		#region Fields

		/// <summary>	The configuration. </summary>
		private readonly IConfiguration _configuration;

		#endregion

		#region Constructors

		public Startup(IHostingEnvironment environment)
		{
			// load configuration
			_configuration = new ConfigurationBuilder()
				.SetBasePath(environment.ContentRootPath)
				.AddJsonFile("appsettings.json", true, true)
				.AddJsonFile("Secrets/appsettings.secret.json", true, true)
				.AddJsonFile($"appsettings.{environment.EnvironmentName}.json", true)
				.AddEnvironmentVariables()
				.Build();
		}

		#endregion

		#region Methods

		/// <summary>	Configure services. </summary>
		/// <exception cref="InvalidOperationException">
		///     Thrown when the requested operation is
		///     invalid.
		/// </exception>
		/// <param name="services">	The services. </param>
		public void ConfigureServices(IServiceCollection services)
		{
			try
			{
				// register configuration
				services.AddSingleton(_configuration);

				// precreate some settings-services
				var loggingSettingsService = new ConfigLoggerSettingsService(_configuration);
				var proxySettingsService = new ConfigProxySettingsService(_configuration);
				var signingSettingsService = new ConfigSigningSettingsService(_configuration);

				// register settings-services
				services.AddSingleton<ILoggerSettingsService, ConfigLoggerSettingsService>(provider => loggingSettingsService);
				services.AddSingleton<IApplicationSettingsService, ConfigApplicationSettingsService>();
				services.AddSingleton<IProxySettingsService, ConfigProxySettingsService>(provider => proxySettingsService);
				services.AddSingleton<IFormsAuthenticationSettingsService, ConfigFormsAuthenticationSettingsService>();
				services.AddSingleton<ISigningSettingsService, ConfigSigningSettingsService>(provider => signingSettingsService);
				services.AddSingleton<IGlobalizationSettingsService, ConfigGlobalizationSettingsService>();
				services.AddSingleton<IAuthenticationSettingsService, ConfigAuthenticationSettingsService>();
				services.AddSingleton<IGoogleOpenIdProviderSettingsService, ConfigGoogleOpenIdProviderSettingsService>();

				// configure identityserver
				var environment = (IHostingEnvironment)services.FirstOrDefault(t => t.ServiceType == typeof(IHostingEnvironment)).ImplementationInstance;
				services.ConfigureIdentityServer(environment, signingSettingsService, proxySettingsService);
			}
			catch (Exception e)
			{
				Console.WriteLine($"Error in {nameof(ConfigureServices)}:");
				Console.WriteLine(e);
				throw;
			}
		}

		/// <summary>	Configures the given application. </summary>
		/// <param name="application">  	The application. </param>
		/// <param name="environment">  	The environment. </param>
		/// <param name="loggerFactory">	The logger factory. </param>
		public void Configure(IApplicationBuilder application, IHostingEnvironment environment, ILoggerFactory loggerFactory)
		{
			// basic config
			application.ConfigureLogging(loggerFactory);
			application.ConfigureProxy();

			// authentication config
			application.ConfigureCookieAuthentication();
			application.UseIdentityServer();
			application.ConfigureGoogleAuthentication();

			// nancy
			application.ConfigureNancy(loggerFactory);

			// auto-start browser
			if (environment.IsDevelopment())
				application.UseBrowser();
		}

		#endregion
	}
}