﻿using FluiTec.AppFx.Service;

namespace FluiTec.Vision.Nancy.Authentication.Owin.Settings
{
	/// <summary>	Interface for google open identifier provider settings service. </summary>
	public interface IGoogleOpenIdProviderSettingsService : ISettingsService<IOpenIdProviderSetting>
	{
	}
}