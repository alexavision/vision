﻿using FluiTec.AppFx.Proxy.Settings;
using FluiTec.AppFx.Service;

namespace FluiTec.AppFx.Proxy.Services
{
	/// <summary>	Interface for proxy settings service. </summary>
	public interface IProxySettingsService : ISettingsService<IProxySettings>
	{
	}
}