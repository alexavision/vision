﻿using System;
using System.Threading.Tasks;
using IdentityServer4.Models;
using IdentityServer4.Stores;

namespace FluiTec.Vision.IdentityServer
{
	/// <summary>	A client store. </summary>
	public class ClientStore : IClientStore
	{
		/// <summary>	Searches for the first client by identifier asynchronous. </summary>
		/// <param name="clientId">	Identifier for the client. </param>
		/// <returns>	The found client by identifier asynchronous. </returns>
		public Task<Client> FindClientByIdAsync(string clientId)
		{
			throw new NotImplementedException();
		}
	}
}