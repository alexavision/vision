﻿using FluiTec.AppFx.Globalization.Settings;
using FluiTec.AppFx.Service;

namespace FluiTec.AppFx.Globalization.Services
{
	/// <summary>	Interface for globalization settings service. </summary>
	public interface IGlobalizationSettingsService : ISettingsService<IGlobalizationSettings>
	{
	}
}