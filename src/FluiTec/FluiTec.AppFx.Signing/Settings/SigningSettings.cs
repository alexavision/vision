﻿using System;
using System.Security.Cryptography.X509Certificates;
using FluiTec.AppFx.Cryptography;

namespace FluiTec.AppFx.Signing.Settings
{
	/// <summary>	A signing settings. </summary>
	public class SigningSettings : ISigningSettings
	{
		/// <summary>	Gets or sets the name of the signing certificate. </summary>
		/// <value>	The name of the signing certificate. </value>
		public string SigningCertificateName { get; set; }

		/// <summary>	Gets the certificate. </summary>
		/// <returns>	The certificate. </returns>
		public X509Certificate2 GetCertificate()
		{
			if (!SslHelper.TryGetCertificate(SigningCertificateName, StoreName.Root, StoreLocation.LocalMachine, out X509Certificate2 cert))
				throw new InvalidOperationException($"Missing Signing-Certificate for IdentityServer. Looking for '{SigningCertificateName}'");
			return cert;
		}
	}
}