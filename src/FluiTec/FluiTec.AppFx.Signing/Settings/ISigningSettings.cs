﻿using System.Security.Cryptography.X509Certificates;

namespace FluiTec.AppFx.Signing.Settings
{
	/// <summary>	Interface for signing settings. </summary>
	public interface ISigningSettings
	{
		/// <summary>	Gets or sets the name of the signing certificate. </summary>
		/// <value>	The name of the signing certificate. </value>
		string SigningCertificateName { get; set; }

		/// <summary>	Gets the certificate. </summary>
		/// <returns>	The certificate. </returns>
		X509Certificate2 GetCertificate();
	}
}