﻿using FluiTec.AppFx.Service;
using FluiTec.AppFx.Signing.Settings;

namespace FluiTec.AppFx.Signing.Services
{
	/// <summary>	Interface for signing settings service. </summary>
	public interface ISigningSettingsService : ISettingsService<ISigningSettings>
	{
	}
}